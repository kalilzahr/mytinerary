const { User, response } = require('../userModule');
const userRepository = require('../../../repositories/userRepository');
const { validationResult } = require('express-validator');
const key = require('../../../keys').secretOrKey;
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');


const signup = async (req, res = response) => {
    const issues = validationResult(req);
    
    if( !(issues.errors.length === 0) ){
        return res.status(400).json({ 
            errors: issues.array() 
        });
    }
    try {
        
        const existingUser = await userRepository.getUserByMail(req.body.email);
        
        if (existingUser) {
            return res.status(400).json({
                ok: false,
                msg: `The mail ${req.body.email} already exists in the Database`
            });
        }

        const newUser = new User(req.body);
        const hashedPassword = await bcrypt.hash(req.body.password, 10);
        newUser.password = hashedPassword;
        
        await userRepository.create(newUser);

        // Create JWT
        const payload = {
            id: newUser.id,
            username: newUser.mail,
            avatarPicture: newUser.userPic
        };

        // Sign JWT
        jwt.sign(
            payload,
            key,
            {expiresIn: 2592000},
            (err, token) => {
                if(err){
                    res.json({
                        success: false,
                        token: 'There was an error'
                    });
                } else {
                    res.status(200).json({ 
                        success: true,
                        response: {
                            message: 'Your account has been created!',
                            token,
                            firstName: newUser.firstName,
                            userPic: newUser.userPic
                        } 
                    });
                }
            });

    } catch (error) {
        res.status(500).json({
            ok: false,
            msg: 'Internal Server Error',
            
        });
    }
};


const signin = async (req, res = response) => {
    // Validate non empty params
    const issues = validationResult(req);
    
    if( !(issues.errors.length === 0) ){
        return res.status(400).json({ 
            errors: issues.array() 
        });
    }

    try {
        const { email, password } = req.body;
        
        // Validate an existing user with that mail
        const existingUser = await userRepository.getUserByMail(email);
        
        if(!existingUser) {
            return res.status(400).json({
                ok: false,
                msg: `The mail ${req.body.mail} does not exists in the Database`
            });
        }

        // Validate correct password
        const correctPassword = await bcrypt.compare(password, existingUser.password);
        
        if(!correctPassword) {
            return res.status(400).json({
                ok: false,
                msg: 'Wrong password'
            });
        }

        // Create JWT
        const payload = {
            id: existingUser.id,
            username: existingUser.mail,
            avatarPicture: existingUser.userPic
        };

        // Sign JWT
        jwt.sign(
            payload,
            key,
            {expiresIn: 2592000},
            (err, token) => {
                if(err){
                    res.json({
                        success: false,
                        token: 'There was an error'
                    });
                } else {
                    res.status(200).json({ 
                        success: true,
                        response: {
                            message: 'Logged In',
                            token,
                            firstName: existingUser.firstName,
                            userPic: existingUser.userPic
                        } 
                    });
                }
            }); 

        

    } catch (error) {
        res.status(500).json({
            succes: false,
            response: {
                message: 'Internal Server Error',
                error
            }
        });
    }
};


module.exports = {
    signup,
    signin
};