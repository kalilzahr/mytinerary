const get = require('./getUsers/getUsers');
const post = require('./createUser/createUser');

module.exports = {
    get,
    post
};