const { response } = require('../userModule');
const userRepository = require('../../../repositories/userRepository');

const getAllUsers = async (req, res = response) => {
    try {
        const usersDb = await userRepository.getAll();
        const count = await userRepository.count();

        if (count === 0) {
            return res.status(404).json({
                ok: false,
                msg: 'There are no users in the Database'
            });
        }

        res.status(200).json({
            ok: true,
            msg: 'Users',
            total: count,
            users: usersDb
        });

    } catch (error) {
        res.status(500).json({
            ok: false,
            msg: 'Internal Server Error',
            error
        });
    }
};


const getUserById = async (req, res = response) => {
    try {
        const userId = req.params.id;
        const userDb = await userRepository.getUserById(userId);

        if (!userDb) {
            return res.status(404).json({
                ok: false,
                msg: `There is no existing user with id: ${req.params.id}`
            });
        }

        res.status(200).json({
            ok: true,
            msg: 'User',
            user: userDb
        });

    } catch (error) {
        res.status(500).json({
            ok: false,
            msg: 'Internal Server Error',
            error
        });
    }
};


const getUserByQuery = async (req, res = response) => {
    try {
        if(req.query.firstName) {
            const firstName = req.query.firstName;
            const userDb = await userRepository.getUserByFirstName(firstName);
            const count = userDb ? userDb.length : 0;
            
            if (count === 0) {
                return res.status(404).json({
                    ok: false,
                    msg: `There are no users with name: ${firstName}`
                });
            }
    
            res.status(200).json({
                ok: true,
                msg: 'Users',
                total: count,
                users: userDb
            });
        }

        if(req.query.lastName) {
            const lastName = req.query.lastName;
            const userDb = await userRepository.getUserByLastName(lastName);
            const count = userDb ? userDb.length : 0;
    
            if (count === 0) {
                return res.status(404).json({
                    ok: false,
                    msg: `There are no users with last name: ${lastName}`
                });
            }
    
            res.status(200).json({
                ok: true,
                msg: 'Users',
                total: count,
                user: userDb
            });
        }

        if(req.query.mail) {
            const mail = req.query.mail;
            const userDb = await userRepository.getUserByMail(mail);
            const count = userDb ? userDb.length : 0;

            if (count === 0) {
                return res.status(404).json({
                    ok: false,
                    msg: `There is no existing user with mail: ${mail}`
                });
            }
    
            res.status(200).json({
                ok: true,
                msg: 'User',
                user: userDb
            });
        }

    } catch (error) {
        res.status(500).json({
            ok: false,
            msg: 'Internal Server Error',
            error
        });
    }
};


const getUserByToken = async (req, res = response) => {
    try {
        const { _id } = req.user;
        
        const userDb = await userRepository.getUserById(_id);
        res.status(200).json({
            succes: true,
            response: {
                userPic: userDb.userPic,
                firstName: userDb.firstName,
            }
        });
        
    } catch (error) {
        res.status(500).json({
            success: false,
            msg: 'Internal Server Error',
            error
        });
    }
};


module.exports = {
    getAllUsers,
    getUserById,
    getUserByQuery,
    getUserByToken
};