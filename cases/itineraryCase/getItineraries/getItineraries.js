const { response } = require('../itineraryModule');
const itineraryRepository = require('../../../repositories/itineraryRepository');
const cityRepository = require('../../../repositories/cityRepository');

const getItineraries = async (req, res = response) => {
    try {
        const itinerariesDb = await itineraryRepository.getAll();
        const count = await itineraryRepository.count();

        if (itinerariesDb.length === 0) {
            return res.status(404).json({
                ok: false,
                msg: 'There are no itineraries in the Database'
            });
        }

        res.status(200).json({
            ok: true,
            msg: 'Itineraries',
            total: count,
            response: itinerariesDb
        });

    } catch (error) {
        res.status(500).json({
            ok: false,
            msg: 'Internal Server Error',
            error
        });
    }
};


const getItineraryById = async (req, res = response) => {
    try {
        const itineraryId = req.params.id;
        const itineraryDb = await itineraryRepository.getOne(itineraryId);

        res.status(200).json({
            ok: true,
            msg: 'Itinerary',
            itinerary: itineraryDb
        });

    } catch (error) {
        return res.status(404).json({
            ok: false,
            msg: 'The ID does not match any existing itinerary in the Database',
            error
        });
    }
};


const getItinerariesByCityId = async (req, res = response) => {
    try {
        const itinerariesByCity = await itineraryRepository.getItinerariesByCityId(req.params.city);
        const city = await cityRepository.getOne(req.params.city);
        const count = itinerariesByCity.length;

        res.status(200).json({
            ok: true,
            msg: `Itineraries in ${city.name} city`,
            total: count,
            response: itinerariesByCity
        });

    } catch (error) {
        return res.status(404).json({
            ok: false,
            msg: 'No itineraries available',
            error
        });
    }
};


module.exports = {
    getItineraries,
    getItineraryById,
    getItinerariesByCityId
};