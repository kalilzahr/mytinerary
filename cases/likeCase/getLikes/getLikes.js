const { response, itineraryRepository, userRepository } = require('../likeModule');

const getLikes = async (req, res = response) => {
    try {
        const { _id } = req.user;
        const itineraryId = req.params.id;

        const itineraryDb = await itineraryRepository.getOne(itineraryId);
        const userDb = await userRepository.getUserById(_id);
        
        let liked;
        if (itineraryDb.usersLike.includes(_id.toString())) { 
            // Remove Like and Favorite
            liked = false;
            itineraryDb.likes--;
            itineraryDb.usersLike = itineraryDb.usersLike.filter( (id) => id !== _id.toString() );
            userDb.favorites = userDb.favorites.filter( (id) => id.toString() !== itineraryId.toString() );
        } else { 
            // Add Like and Favorite
            liked = true;
            itineraryDb.likes++;
            itineraryDb.usersLike.push(_id.toString());
            userDb.favorites.push(itineraryId);
        }
        
        await itineraryRepository.saveItinerary(itineraryDb);
        await userRepository.create(userDb);
        
        const likes = itineraryDb.likes;
        
        res.status(200).json({
            success: true,
            response: {
                likes,
                liked
            }
        });

    } catch (error) {
        res.status(500).json({
            success: false,
            msg: 'Internal Server Error',
            error
        });
    }
};

module.exports = {
    getLikes
};