const { response, itineraryRepository } = require('../checkUserModule');

const getCommentsByUser = async (req, res = response) => {
    try {
        const { _id } = req.user;
        const itineraryId = req.params.id;

        const itineraryDb = await itineraryRepository.getOne(itineraryId);

        // Get all the comments from the user
        const comments = [];
        itineraryDb.comments.map((comment) => (comment.userId.toString() === _id.toString()) ? comments.push(comment._id) : null);

        // Check for likes
        let liked = false;
        if (itineraryDb.usersLike.includes(_id.toString())) liked = true;
        
        
        res.status(200).json({
            success: true,
            response: {
                arrayOwnerCheck: comments,
                likedChek: liked
            }
        });

    } catch (error) {
        res.status(500).json({
            ok: false,
            msg: 'Internal Server Error',
            error
        });
    }
};

module.exports = {
    getCommentsByUser
};