const { response, itineraryRepository } = require('../commentModule');
const { validationResult } = require('express-validator');

const createComment = async (req, res = response) => {

    const errores = validationResult(req);
    if( !errores.isEmpty() ){
        return res.status(400).json({ errores: errores.array() });
    }

    try {
        const itineraryId = req.params.id;
        const user = req.user;
        const commentText = req.body.text; 
        
        const itineraryDb = await itineraryRepository.getOne(itineraryId);
        itineraryDb.comments.push({
            userId: user._id,
            text: commentText,
            userName: user.firstName + ' ' + user.lastName,
            userPic: user.userPic
        });
        
        // Search comments by user id
        const userComments = [];
        itineraryDb.comments.map((comment) => (comment.userId.toString() === user._id.toString()) ? userComments.push(comment._id) : null );
        
        await itineraryRepository.saveItinerary(itineraryDb);

        res.status(200).json({
            success: true,
            response: itineraryDb.comments,
            arrayUserComments: userComments
        });

    } catch (error) {
        res.status(500).json({
            success: false,
            msg: 'Internal Server Error',
            error
        }); 
    }
};

module.exports = {
    createComment
};