const post = require('./createComment/createComment');
const del = require('./deleteComment/deleteComment');
const put = require('./modifyComment/modifyComment');

module.exports = {
    post,
    del,
    put
};