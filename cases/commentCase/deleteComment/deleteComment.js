const { response, itineraryRepository } = require('../commentModule');

const deleteComment = async (req, res = response) => {
    try {
        const commentId = req.params.id;
        const itinerariesDb = await itineraryRepository.getAll();

        let itineraryFound;
        itinerariesDb.map( async (itinerary) => {
            itinerary.comments.map( (comment) => (comment._id.toString() === commentId.toString()) ? itineraryFound = itinerary : null );
        });

        itineraryFound.comments = itineraryFound.comments.filter( (comment) => comment._id.toString() !== commentId.toString() );

        await itineraryRepository.saveItinerary(itineraryFound);

        const commentsArray = itineraryFound.comments;

        res.status(200).json({
            success: true,
            response: commentsArray
        });
    }
    catch (error) {
        return res.status(500).json({
            success: false,
            error
        });
    }
};

module.exports = {
    deleteComment
};