const { response, itineraryRepository } = require('../commentModule');

const modifyComment = async (req, res = response) => {
    try {
        const commentId = req.params.id;
        const commentText = req.body.text;

        const itinerariesDb = await itineraryRepository.getAll();

        let itineraryFound;
        let commentsArray = [];
        itinerariesDb.map( async (itinerary) => {
            itinerary.comments.map( (comment) => {
                if(comment._id.toString() === commentId.toString()) {
                    comment.text = commentText;
                    itineraryFound = itinerary;
                    commentsArray = itinerary.comments;
                }
            });
        });

        await itineraryRepository.saveItinerary(itineraryFound);

        res.status(200).json({
            success: true,
            response: commentsArray
        });
    }
    catch (error) {
        return res.status(500).json({
            success: false,
            error
        });
    }
};

module.exports = {
    modifyComment
};