const { City, response } = require('../cityModule');
const cityRepository = require('../../../repositories/cityRepository');

const createCity = async (req, res = response) => {
    const { name } = req.body;
    try {
        const cityFound = await cityRepository.getCityByName(name);
        
        if (cityFound.length !== 0) return res.status(400).json({
            ok: false,
            msg: `The city ${name} already exists in the Database`
        });

        const newCity = new City({
            name: req.body.name,
            country: req.body.country,
            img:  req.body.img
        });

        await newCity.save();
        res.json({
            ok: true,
            msg: 'The city was added to the Database',
            city: newCity
        });

    } catch (error) {
        res.status(500).send({
            ok: false,
            msg: 'Internal Server Error',
            error
        });
    }
};

module.exports = {
    createCity
};