const { response } = require('../cityModule');
const cityRepository = require('../../../repositories/cityRepository');

const getCities = async (req, res = response) => {
    try {
        const citiesDb = await cityRepository.getAll();
        const count = await cityRepository.count();

        if (citiesDb === 0) {
            return res.status(404).json({
                ok: false,
                msg: 'There are no cities in the Database'
            });
        }

        res.status(200).json({
            ok: true,
            msg: 'Cities',
            total: count,
            response: citiesDb
        });

    } catch (error) {
        res.status(500).json({
            ok: false,
            msg: 'Internal Server Error',
            error
        });
    }
};


const getCityById = async (req, res = response) => {
    try {
        const cityId = req.params.id;
        const cityDb = await cityRepository.getOne(cityId);

        res.status(200).json({
            ok: true,
            msg: 'City',
            city: cityDb
        });

    } catch (error) {
        return res.status(404).json({
            ok: false,
            msg: 'The ID does not match any existing city in the Database',
            error
        });
    }
};


const getCityByQuery = async (req, res = response) => {
    try {
        if(req.query.name) {
            const name = req.query.name;
            const cityDb = await cityRepository.getCityByName(name);
    
            if (cityDb.length === 0) {
                return res.status(404).json({
                    ok: false,
                    msg: `The city ${name} does not exist in the Database`
                });
            }
    
            res.status(200).json({
                ok: true,
                msg: 'City',
                city: cityDb
            });
        }

        if(req.query.country) {
            const country = req.query.country;
            const citiesDb = await cityRepository.getCitiesByCountry(country);
            const count = citiesDb.length;

            if(count === 0) {
                return res.status(404).json({
                    ok: false,
                    msg: `There are not cities in ${country}`
                });
            }

            res.status(200).json({
                ok: true,
                msg: 'Cities',
                total: count,
                cities: citiesDb
            });
        }

    } catch (error) {
        res.status(500).json({
            ok: false,
            msg: 'Internal Server Error',
            error
        });
    }
};


module.exports = {
    getCities,
    getCityById,
    getCityByQuery
};