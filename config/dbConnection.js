const mongoose = require('mongoose');
const db = require('../keys').mongoURI;

const dbConnection = async () => {
    mongoose.connect(db, {
        useNewUrlParser: true, 
        useUnifiedTopology: true
    })
    
        .then(() => console.log ('Conexión a MongoDB establecida'))
    
        .catch(err => console.log (err));

};

module.exports = {
    dbConnection
};