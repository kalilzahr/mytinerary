const City  = require('../database/models/City');

const getAll  = async ()  =>  await City.find();
const count = async ()  =>  await City.count();
const getOne  = async id  =>  await City.findById(id);
const getCityByName = async name  =>  await City.find({name:name});
const getCitiesByCountry = async country  =>  await City.find({country:country});

module.exports  = {
    getAll,
    count,
    getOne,
    getCityByName,
    getCitiesByCountry
};