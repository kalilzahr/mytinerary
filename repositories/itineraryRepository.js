const Itinerary = require('../database/models/Itinerary');

const getAll = async () => await Itinerary.find();
const count = async () => await Itinerary.count();
const getOne = async id => await Itinerary.findById(id);
const getItinerariesByCityId = async cityId => await Itinerary.find({cityId: cityId});

const saveItinerary = async (itinerary) => await itinerary.save();

module.exports = {
    getAll,
    count,
    getOne,
    getItinerariesByCityId,
    saveItinerary
};