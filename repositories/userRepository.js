const User = require('../database/models/User');

const getAll = async () => await User.find({});
const count = async () => await User.count();
const getUserById = async (id) => await User.findById(id);
const getUserByFirstName = async (firstName) => await User.findOne({firstName: firstName});
const getUserByLastName = async (lastName) => await User.findOne({lastName: lastName});
const getUserByMail = async (email) => await User.findOne({email: email});

const create = async (user) => await user.save();

module.exports = {
    getAll,
    count,
    getUserById,
    getUserByFirstName,
    getUserByLastName,
    getUserByMail,
    create
};