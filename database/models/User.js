const mongoose = require('mongoose');
const { ObjectID } = require('mongodb');

const UserSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: true,
        trim: true
    },

    lastName: {
        type: String,
        required: true,
        trim: true
    },

    password: {
        type: String,
        required: true,
        trim: true
    },

    email: {
        type: String,
        required: true,
        trim: true
    },

    userPic: {
        type: String,
        required: true,
        trim: true
    },

    country: {
        type: String,
        required: true,
        trim: true
    },

    favorites: {
        type: [{ 
            type: ObjectID, 
            ref: 'Itinerary' 
        }]
    }
});

module.exports = mongoose.model('User', UserSchema);