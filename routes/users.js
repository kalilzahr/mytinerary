const { Router } = require('../cases/userCase/userModule');
const router = new Router();
const { get, post } = require('../cases/userCase/userController');
const { check } = require('express-validator');
const passport = require('../passport');

// const registerSchema = require('../joiSchemas/register');
// const loginSchema = require('../joiSchemas/login');
// const { validateData } = require('../middleware/validateData');

// GET requests
router.get('/users', get.getAllUsers);
router.get('/user/:id', get.getUserById);
router.get('/user', get.getUserByQuery);
router.get('/users/signinls',
    passport.authenticate('jwt', {session: false}),
    get.getUserByToken
);


// POST requests
router.post('/user/signup', 
    [
        check('firstName', 'First name was not provided').isString().not().isEmpty(),
        check('lastName', 'Last name was not provided').isString().not().isEmpty(),
        check('password', 'Password was not provided').not().isEmpty(),
        check('email', 'Must provide a valid mail').isEmail(),
        check('userPic', 'User pic was not provided').isString().not().isEmpty(),
        check('country', 'Country was not provided').isString().not().isEmpty()
    ], 
    post.signup
);

router.post('/user/signin',
    [
        check('email', 'Must provide a valid mail').isEmail(),
        check('password', 'Password was not provided').not().isEmpty()
    ],
    post.signin
);


module.exports = router;