const { Router } = require('../cases/itineraryCase/itineraryModule');
const router = new Router();
const { get } = require('../cases/itineraryCase/itineraryController');

// GET requests
router.get('/itineraries', get.getItineraries);
router.get('/itinerary/:id', get.getItineraryById);
router.get('/itineraries/:city', get.getItinerariesByCityId);

module.exports = router;