const { Router } = require('../cases/likeCase/likeModule');
const router = new Router();
const { get } = require('../cases/likeCase/likeController');
const passport = require('../passport');

router.get('/like/:id',
    passport.authenticate('jwt', {session: false}),
    get.getLikes
);

module.exports = router;