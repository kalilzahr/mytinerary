const { Router } = require('../cases/cityCase/cityModule');
const router = new  Router();
const { create, get  } = require('../cases/cityCase/cityController');

// GET requests
router.get('/cities',  get.getCities);
router.get('/city/:id', get.getCityById); 
router.get('/city', get.getCityByQuery);

// POST requests
router.post('/city', create.createCity);
  
module.exports = router;