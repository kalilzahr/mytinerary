const { Router } = require('../cases/commentCase/commentModule');
const router = new Router();
const { post, del, put } = require('../cases/commentCase/commentController');
const passport = require('../passport');
const { check } = require('express-validator');

// Create comment
router.post('/comments/:id',
    passport.authenticate('jwt', {session: false}),
    [
        check('text', 'Must add a valid text').isString().not().isEmpty(),
    ], 
    post.createComment
);


// Delete Comment
router.delete('/comment/:id', 
    passport.authenticate('jwt', {session: false}),
    del.deleteComment
);


// Modify Comment
router.put('/comment/:id',
    passport.authenticate('jwt', {session: false}),
    [
        check('text', 'Must add a valid text').isString().not().isEmpty(),
    ], 
    put.modifyComment
);

module.exports = router;