const { Router } = require('../cases/checkuserCase/checkuserModule');
const router = new Router;
const { get } = require('../cases/checkuserCase/checkuserController');
const passport = require('../passport');

router.get('/checkuser/:id', 
    passport.authenticate('jwt', {session: false}),
    get.getCommentsByUser
);

module.exports = router;