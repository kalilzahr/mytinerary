const { app, port} = require('./app');

// Passport configuration
require('./passport');

app.listen (port, () => {

    console.log ('El servidor se está ejecutando en el port ' + port);

});