<h1 style="align: center;">myTinerary Backend App</h1>

<h3>Steps:</h3>
<pre>1. 'npm start' to run the server</pre>
<pre>2. Clone the [Front End repository](https://gitlab.com/kalilzahr/mytinerary-frontend/-/tree/main)</pre>
<pre>3. In the Front End repository 'npm install' to install all the dependencies</pre>
<pre>4. In the Front End repository 'npm update --force' to update the dependencies to their latest versions</pre>
<pre>5. In the Front End repository 'npm start' to run the Front End</pre>

<br/>

<p style="align: center;"> 
    <a href="https://www.mongodb.com/" target="_blank"> 
        <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mongodb/mongodb-original-wordmark.svg" alt="mongodb" width="40" height="40"/> 
    </a> 
    <a href="https://nodejs.org" target="_blank"> 
        <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/nodejs/nodejs-original-wordmark.svg" alt="nodejs" width="40" height="40"/> 
    </a> 
    <a href="https://expressjs.com/es/" target="_blank"> 
        <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/express/express-original-wordmark.svg" alt="express" width="40" height="40"/> 
    </a> 
</p>