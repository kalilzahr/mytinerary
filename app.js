const express = require ('express');
const app = express ();
const port = process.env.PORT || 4000;

const cors = require('cors');
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

// DB CONNECTION
const { dbConnection } = require('./config/dbConnection');
dbConnection();

// Routes
app.use('/api', require('./routes/cities'));
app.use('/api', require('./routes/itineraries'));
app.use('/api', require('./routes/users'));
app.use('/api', require('./routes/checkUser'));
app.use('/api', require('./routes/comments'));
app.use('/api', require('./routes/likes'));

module.exports = {
    app,
    port
};